const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const Schema = mongoose.Schema;

const UsuarioSchema = new Schema({
  usuario: { type: String, required: true, unique: true },
  senha: { type: String, required: true, select: false },
  token: { type: String}
});

//Hook pre-save para encriptar a senha antes de salvar no banco
UsuarioSchema.pre('save', function() {

  if(this.isModified('senha')){
    bcrypt.hash(this.senha, null, null, (erro, senha) => {
      if(erro) throw erro;

      this.senha = senha;
    })
  }
});

UsuarioSchema.methods.compararSenha = function(senha){
  
  return bcrypt.compareSync(senha, this.senha);;
}

module.exports = mongoose.model('Usuario', UsuarioSchema);