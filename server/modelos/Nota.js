const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NotaSchema = new Schema({
  id_usuario: { type: String, required: true, select: false },
  titulo: {type: String, required: true},
  conteudo: {type: String, required: true},
})

module.exports = mongoose.model('Nota', NotaSchema);