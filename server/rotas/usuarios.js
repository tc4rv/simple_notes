const express = require('express');
const rotasUsuarios = express.Router();
const { Usuario } = require('../modelos');
const { ValidadorUsuario } = require('../validadores');
const { erroHandler } = require('../handlers');

rotasUsuarios.post('/', ValidadorUsuario.validarUsuario, erroHandler(async (req, res) => {
  const { usuario, senha } = req.body;

  const novoUsuario = new Usuario({ usuario, senha });

  await novoUsuario.save();

  res.send({sucesso: true});

}));

rotasUsuarios.post('/login', erroHandler(async (req, res, next) => {
  const { usuario, senha } = req.body;

  if (!usuario || !senha)
    return next({ statusHttp: 400, mensagem: 'Os campos usuário e senha são obrigatórios!' });

  const usuarioSelecionado = await Usuario.findOne({ usuario: usuario }, '+senha');

  if (!usuarioSelecionado.compararSenha(senha))
    return next({ statusHttp: 400});

  //Gerar token e setar num cookie

  const token = ValidadorUsuario.gerarToken({ id: usuarioSelecionado._id });

  usuarioSelecionado.token = token;
  await usuarioSelecionado.save();

  res.cookie('token', token, { httpOnly: true }).send({sucesso: true});
}));

rotasUsuarios.get('/sair', erroHandler(async (req, res, next) => {
  if(req.cookies.token){
    const token = await ValidadorUsuario.validarToken(req.cookies.token);

    if(!token.id)
      return next(token);

    const usuario = await Usuario.findOne({ _id: token.id });

    if(!usuario)
      throw {statusHttp: 400, mensagem: 'SAI DAQUIIIII!'};

    usuario.token = '';
    usuario.save();

    res.cookie('token', '', {expires: new Date(), httpOnly: true});
  }

  res.send({sucesso: true});
}));

module.exports = rotasUsuarios;