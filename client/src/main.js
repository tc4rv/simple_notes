
import Vue from 'vue'
import App from './App.vue';
import Roteador from './rotas';
import loja from './store';
import Vuelidate from 'vuelidate';
import { debuglog } from 'util';

Vue.config.productionTip = false;

const roteador = Roteador(Vue);
Vue.use(Vuelidate);

const vue = new Vue({
  el: '#app',
  router: roteador,
  store: loja,
  render: h => h(App)
});