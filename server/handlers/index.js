//Função que faz um wrap de todas os middlewares com Promises para não ter que ficar fazendo try-catch em tudo
function erroHandler(funcaoAssincrona){
  return async (req, res, next) => {
    try{
      await funcaoAssincrona(req, res, next);
    }catch(erro){
      next(erro);
    }
  }
}

//Função que determina o que comprimir na response
function filter(req, res){
  const encoding = res.getHeader('Content-Type');

  try{
    if(encoding.length){
      const regex = new RegExp(/^(image|video|audio)/, 'i');

      if(regex.test(encoding))
        return true;
      else
        return false;
    }
  }
  catch(erro){
    return false;
  }
}


module.exports = {
  erroHandler: erroHandler,
  filter: filter
}