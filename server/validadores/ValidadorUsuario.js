const Joi = require('joi');
const jwt = require('jsonwebtoken');
const segredo = 'esseSegredEhPikka!';

const UsuarioSchema = {
  usuario: Joi.string().regex(/^\w{3,32}$/).required(),
  senha: Joi.string().regex(/^\S{4,}$/).required()
}

const mensagens = {
  usuario: 'Usuário deve ter entre 3 e 32 caracteres (letras, números e _) sem espaços!',
  senha: 'A senha deve ter no mínimo 4 caracteres (letras, números e caracteres especiais) e não deve conter espaço'
}

function validarUsuario(req, res, next){
  Joi.validate(req.body, UsuarioSchema, {abortEarly: false}, (erro, usuario) => {
    if(erro) {
      const erros = erro.details.map(erro => mensagens[erro.path[0]]);

      next({statusHttp: 400, mensagem: erros});
    }

    req.body = usuario;
    next();
  });
}

function gerarToken(dados){
  return jwt.sign(dados, segredo);
}

function validarToken(token){
  return new Promise((resolve, reject) => {
    try{
      const decoded = jwt.verify(token, segredo);

      resolve(decoded);

    } catch(erro) {

      reject({statusHttp: 400, mensagem: 'Você não pode continuar! Faça login!'});
    }
  });
}

module.exports = {
  validarUsuario: validarUsuario,
  gerarToken: gerarToken,
  validarToken: validarToken
}