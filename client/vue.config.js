const fs = require('fs');
const PreloadPlugin = require('@vue/preload-webpack-plugin');
const config = require('../server/config');

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? '/public/' : '/',
  outputDir: process.env.NODE_ENV === 'production' ? '../server/public' : '/dist',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000'
      }
    },
    public: '192.168.1.2:8080',
    disableHostCheck: true,
    watchOptions: {
      poll: true
    }
  },

  chainWebpack: (config) => {

    config.plugin('preloadFont')
      .use(PreloadPlugin, [{
        rel: 'preload',
        as: 'font',
        include: 'allAssets',
        fileWhitelist: [/\.(woff2?|eot|ttf|otf)$/]
      }])
      .before('preload');

    // config.module
    //   .rule('fonts')
    //   .use('url-loader')
    //   .tap((opcoes) => {
    //     opcoes.limit = 36884;

    //     return opcoes;
    //   })

    fs.writeFile('./chain_webpack_config.js', config.toString(), (erro) => { if (erro) console.log(erro) });
  }
}