const config = {
  stringConexaoDB: process.env.NODE_ENV === 'production' ? 'mongodb://simple_notes:sSN561@ds117535.mlab.com:17535/simple_notes' : 'mongodb://localhost/simple_notes',

  port: process.env.PORT || 3000
}

module.exports = config;