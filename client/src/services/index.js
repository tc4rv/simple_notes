import usuariosAPI from './usuariosAPI';
import notasAPI from './notasAPI';

export {
  usuariosAPI,
  notasAPI
}