const Joi = require('joi');

const NotaSchema = Joi.object().keys({
  id_usuario: Joi.string().required(),
  titulo: Joi.string().required(),
  conteudo: Joi.string().required()
});

const mensagens = {
  id_usuario: 'Não foi possível validar a nota',
  titulo: 'A nota precisa de um título!',
  conteudo: 'A nota precisa de um conteúdo!'
}

function validarNota(req, res, next){
  Joi.validate({id_usuario: req.decoded.id, ...req.body}, NotaSchema, {abortEarly: false}, (erro, notaValidada) => {
    if (erro) {
        const erros = erro.details.map(erro => mensagens[erro.path[0]]);

        next({statusHttp: 400, mensagem: erros});
    }

    req.body = {
      id_usuario: req.decoded.id,
      ...notaValidada
    };

    next();
  });
}

module.exports = {
  validarNota: validarNota
};