const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const MongooseError = mongoose.Error;
const MongoError = mongoose.mongo.MongoError;
const compression = require('compression');
const config = require('./config');

process.env.NODE_ENV = 'production';

//Importa as rotas
const rotas = require('./rotas');

//conecta com o banco de dados
mongoose.connect(config.stringConexaoDB, {useNewUrlParser: true}, (erro) => {
  if(erro) return console.log(erro.message);

  console.log('Conectado ao banco!');
});

const server = express();

//Seta os middlewares de CORS, de parser do body da requisição e da queryString da url, dos cookies e de compressão
server.use(cors({origin: true, credentials: true}));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));
server.use(cookieParser());
server.use(compression({filter: () => true}));

//Informa o middleware para arquivos estáticos
    server.use('/public', express.static('./public'));

// server.use((req, res, next) => {res.removeHeader('X-Powered-By'); next()});

//Configura as rotas de usuarios e notas
server.use('/api/usuarios', rotas.usuarios);
server.use('/api/notas', rotas.notas);

//Gerencia qualquer erro que for lançado
server.use((erro, req, res, next) => {

  let Erro = {
    sucesso: false,
    statusHttp: erro.statusHttp || 500,
    mensagem: erro.mensagem || "Não foi possível executar a operação"
  }

  if(erro instanceof MongooseError || erro instanceof MongoError){

    if(erro instanceof MongooseError){
      if(erro.name === 'CastError'){
        Erro.mensagem = 'Não foi possível encontrar a nota!';
        Erro.statusHttp = 400;
      }
      else {
        Erro.mensagem = erro.message;
      }

    } else {

      if(erro.code === 11000){
        Erro.statusHttp = 400;
        Erro.mensagem = "Já existe um usuário com este nome. Por favor, escolha outro!";
      }
      else {
        Erro.mensagem = erro.errmsg;
      }
    }
  }

  res.status(Erro.statusHttp).send({sucesso: false, mensagem: Erro.mensagem});
});

//Manda todo resto de requisição para o SPA (Vuejs)
server.all('*', (req, res, next) => {
  require('fs').readFile('./public/index.html', {encoding: 'utf8'}, (erro, arquivo) => {    
    if(erro)
      throw next(erro);
    
    res.set('Content-Type', 'text/html');

    res.send(arquivo);
  });
});

server.listen(config.port, () => console.log(`Ounvindo em localhost:${config.port}`));

console.log(config.stringConexaoDB);