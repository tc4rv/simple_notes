import VueRouter from 'vue-router';
import store from '../store';

// import Home from '../components/Home/Home';
// import Login from '../components/Login';
// import Registro from '../components/Registro';
// import Sobre from '../components/Sobre';
// import Notas from '../components/Nota/Notas';
// import Nota from '../components/Nota/Nota';
// import { timingSafeEqual } from 'crypto';

// Esta função faz o webpack gerar mini arquivos que são requisitados APENAS quando soliticados
function carregarComponente(component){
  return () => import(`../components/${component}.vue`);
}

const rotas = [

  {path: "/", component: carregarComponente('Home')},

  {path: '/login', component: carregarComponente('Login')},

  {path: '/registro', component: carregarComponente('Registro')},

  {path: '/sobre', component: carregarComponente('Sobre')},

  //As rotas que começam com '/notas' devem ser protegidas e funcionarem apenas se o usuário estiver logado!
  {path: '/notas', component: carregarComponente('Nota/Notas'), meta: {autenticacao: true}, beforeEnter: validar},

  {
    path: '/notas/novaNota',
    component: carregarComponente('Nota/Nota'),
    meta: {autenticacao: true}
},

  {path: '/notas/:id_nota', component: carregarComponente('Nota/Nota'), meta: {autenticacao: true}, beforeEnter: validar},

  {path: '*', beforeEnter: (to,from, next) => {
    store.dispatch('mensagem', {erro: true, mensagem: 'Recurso não encontrado!', tempo: 3000});

    next('/');
  }}
];

async function validar(to, from, next){
  let resultado = await store.dispatch('set_notas', to.path === '/notas' ? '' : `/${to.params.id_nota}`);

  if(resultado.sucesso){
    
    next();

  } else {

    await store.dispatch('mensagem', {erro: true, mensagem: !resultado.sucesso ? resultado.mensagem : 'Você precisa estar autenticado!', tempo: 3000});
    next('/login');
  }
}

export default function (vue){
  vue.use(VueRouter);

  const roteador = new VueRouter({
    mode: 'history',
    routes: rotas
  });

  roteador.beforeEach(async (to, from, next) => {
    const autenticado = await store.dispatch('checarAutenticacao');

    if(to.path.match(/\/(login|registro)/) && autenticado){
      await store.dispatch('mensagem', {erro: true, mensagem: `Você já ${to.path === '/login' ? 'está logado!' : 'possui registro!'}`, tempo: 3000});
      return next(false);
    }
    
    next();
  });

  roteador.afterEach((to, from) => {
    if(store.state.mensagem && !store.state.idMensagem)
      store.dispatch('mensagem', {erro: '', mensagem: ''});
  });

  return roteador;
}