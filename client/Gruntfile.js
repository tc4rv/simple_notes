// função que cria x imagens com y tamanhos e z qualidade
function retornaTamanhos(detalhes){
  let resultado = [];

  detalhes.tamanhos.forEach((tamanho) => {
    resultado.push({width: tamanho, quality: detalhes.qualidades[0]});

    if(detalhes.outrosDPI.length)
      detalhes.outrosDPI.forEach((dpi, indice) => resultado.push({width: tamanho * dpi, quality: detalhes.qualidades[indice + 1]}));
  });

  return resultado;
}

module.exports = (grunt) => {
  grunt.initConfig({
    responsive_images: {
      dev: {
        options:{
          engine: 'im',

          sizes: retornaTamanhos({tamanhos: [300, 500, 800], outrosDPI: [2], qualidades: [75, 50]})
        },

        files: [
          {
            expand: true,
            cwd: 'assets/fotos/teste/',
            src: '*.{jpeg,jpg,png,gif,svg}',
            dest: 'assets/fotos/',
          },
          {
            expand: true,
            cwd: 'assets/fotos/teste',
            src: '*.{jpeg,jpg,png,gif,svg}',
            dest: 'assets/fotos/webp/',
            ext: '.webp',
            extDot: 'last'
          }]
        }
      }
  });

  grunt.loadNpmTasks('grunt-responsive-images');

  grunt.registerTask('default', ['responsive_images']);
}