import Vuex from 'vuex';
import Vue from 'vue';

//importa os services de usuario e nota
import {usuariosAPI, notasAPI} from '../services';

//Store é como se fosse o data de cada componente vue
const state = {
  usuario: null,
  notas: null,

  erro: '',
  mensagem: '',
  idMensagem: ''
}

//Getters são como as computed properties da instância Vue
const getters = {

}

//Apenas Mutations mudam o Estado da aplicação diretamente
const mutations = {
  SET_USUARIO: (state, nome) => {
    state.usuario = nome;
  },

  SET_NOTAS: (state, dados) => {
    state.notas = dados
  },

  SET_MENSAGEM: (state, valores) => {
    state.mensagem = valores.mensagem;
    state.erro = valores.erro;
  },

  SET_ID_MENSAGEM(state, id){
    state.idMensagem = id;
  }
}

const actions = {
  async logar(contexto, dados){
    const resultado = await usuariosAPI.logar(dados);

    return resultado;
  },

  async set_notas(contexto, caminho){
    const resultado = await notasAPI.getNotas(caminho);

    if(!resultado.sucesso) {
      await contexto.dispatch('sair');
  
    } else {
      contexto.commit('SET_NOTAS', resultado.mensagem);
      contexto.commit('SET_USUARIO', resultado.usuario);
    }
    return resultado;
  },

  async incluirNota(contexto, dados){
    const resultado = await notasAPI.incluirNota(dados)

    if(!resultado.sucesso)
      await contexto.dispatch('sair');
    
    return resultado;
  },

  async atualizarNota(contexto, dados){
    const resultado = await notasAPI.atualizarNota(dados);

    if(!resultado.sucesso)
      await contexto.dispatch('sair');

    return resultado;
  },

  async excluirNota(contexto, id){
    const resultado = await notasAPI.excluirNota(id);

    if(!resultado.sucesso)
      await contexto.dispatch('sair');

    return resultado;
  },

  mensagem(contexto, dados){
    contexto.commit("SET_MENSAGEM", dados);

    if(dados.tempo){
      if(contexto.state.idMensagem)
        clearTimeout(contexto.state.idMensagem);
        
      contexto.commit('SET_ID_MENSAGEM', setTimeout(() => {
        contexto.commit('SET_MENSAGEM', {erro: "", mensagem: ""});
      }, dados.tempo));
    }
  },

  async sair(contexto){
    contexto.commit("SET_USUARIO", null);

    contexto.commit('SET_NOTAS', null);
    
    await usuariosAPI.sair();
  },

  checarAutenticacao(contexto){
    return contexto.state.usuario;
  }
}

Vue.use(Vuex);

const loja = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});

export default loja;
