const express = require('express');
const rotasNotas = express.Router();
const { Nota, Usuario } = require('../modelos');
const { ValidadorNota, ValidadorUsuario } = require('../validadores');
const {erroHandler} = require('../handlers');
const compression = require('compression');

//middleware que protege toda e qualquer operação sobre as notas.
rotasNotas.use(erroHandler(async (req, res, next) => {

  if (!req.cookies.token)
    throw {statusHttp: 400, mensagem: 'É preciso estar autenticado!'};

    const token = await ValidadorUsuario.validarToken(req.cookies.token);
    const usuario = await Usuario.findOne({_id: token.id}).where({token: req.cookies.token});

    if(!usuario)
      throw {statusHttp: 400, mensagem: 'Você não pode continuar. Faça login!'};

    req.decoded = token;
    req.usuario = usuario.usuario;

    next();
}));

rotasNotas.route('/')
  .get(erroHandler(async (req, res) => {

    const token = req.decoded;

    const notas = await Nota.find({ id_usuario: token.id });

    if (!notas.length)
      return res.send({sucesso: true, mensagem: 'Você ainda não possui notas!', usuario: req.usuario});

    res.send({sucesso: true, mensagem: notas, usuario: req.usuario});
  }))

  .post(ValidadorNota.validarNota, erroHandler(async (req, res) => {

    const novaNota = new Nota(req.body);
    await novaNota.save();

    res.send({sucesso: true});
  }));

//IMPLEMENTAR ROTA /:id_nota !!!
rotasNotas.route('/:id_nota')
  .get(erroHandler(async (req, res) => {
    const { id_nota } = req.params;
    const token = req.decoded;

    const nota = await Nota.findOne({ _id: id_nota }).where({ id_usuario: token.id });

    if (!nota)
      throw { statusHttp: 400, mensagem: 'Nota não encontrada!' };

    res.send({sucesso: true, mensagem: nota, usuario: req.usuario});
  }))

  .put(erroHandler(async (req, res) => {
    const { id_nota } = req.params;
    const { titulo, conteudo } = req.body;

    const nota = await Nota.findOne({ _id: id_nota }).where({ id_usuario: req.decoded.id });

    if (!nota)
      throw { statusHttp: 400, mensagem: "Nota não encontrada!" }

    nota.titulo = titulo || nota.titulo;
    nota.conteudo = conteudo || nota.conteudo;

    if (nota.isModified())
      await nota.save();

    res.send({sucesso: true});
  }))

  .delete(erroHandler(async (req, res) => {

    const token = req.decoded;
    const { id_nota } = req.params;

    const resultado = await Nota.findOneAndDelete({ _id: id_nota }).where({ id_usuario: token.id });

    if (!resultado)
      throw { statusHttp: 400, mensagem: 'Nota não encontrada!' };

    res.send({sucesso: true});
  }));

module.exports = rotasNotas;