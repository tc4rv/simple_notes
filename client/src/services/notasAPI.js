import axios from 'axios';

const api = axios.create({baseURL: '/api/notas', withCredentials: true});

function getNotas(caminho) {
  return api.get(caminho)
    .then(resposta => resposta.data)

    .catch(erro => erro.response.data);
}

function incluirNota(dados){
  return api.post('', dados)
    .then((resposta) => resposta.data)
    .catch((erro) => erro.response.data);
}

function atualizarNota({id, titulo, conteudo}){
  return api.put(id, {titulo, conteudo})
    .then((resposta) => resposta.data)
    .catch((erro) => erro.response.data);
}

function excluirNota(id){
  return api.delete(id)
    .then((resposta) => resposta.data)
    .catch((erro) => erro.response.data);
}

export default {
  getNotas,
  incluirNota,
  atualizarNota,
  excluirNota
}