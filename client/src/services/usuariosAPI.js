import axios from 'axios';

const api = axios.create({baseURL: '/api/usuarios', withCredentials: true});

//Rotas de usuário
function logar(dados){
  return api.post('/login', dados)
    .then((resposta) => resposta.data)

    .catch((erro) => erro.response.data)
}

function adicionarUsuario(dados){
  return api.post('', dados)
    .then(resposta => resposta.data)

    .catch(erro => erro.response.data);
}

function sair(){
  return api.get('/sair')
    .then((resposta) => resposta.data)

    .catch((erro) => erro.response.data);
}

export default {
  logar: logar,
  adicionarUsuario: adicionarUsuario,
  sair: sair
}